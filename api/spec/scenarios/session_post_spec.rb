describe "POST /sessions" do
  context "login com sucesso" do
    before (:all) do
      payload = { email: "fon@gmail.com", password: "scsleo86" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end
end

# examples = [
#   {
#     title: "senha invalida",
#     payload: { email: "leoscs86@gmail.com", password: "##$%%" },
#     code: 401,
#     error: "Unauthorized",
#   },
#   {
#     title: "senha em branco",
#     payload: { email: "efraim@gmail.com", password: "" },
#     code: 412,
#     error: "required password",
#   },
#   {
#     title: "sem o campo senha",
#     payload: { email: "leoscs86@gmail.com" },
#     code: 412,
#     error: "required password",
#   },
#   {
#     title: "sem o campo email",
#     payload: { password: "scsleo86" },
#     code: 412,
#     error: "required email",
#   },
#   {
#     title: "email em branco",
#     payload: { email: "", password: "scsleo86" },
#     code: 412,
#     error: "required email",
#   },
#   {
#     title: "usuario não existe",
#     payload: { email: "aeiou@gmail.com", password: "scsleo86" },
#     code: 401,
#     error: "Unauthorized",
#   },
# ]

# puts examples.to_json

examples = Helpers::get_fixture("login")

examples.each do |e|
  context "#{e[:title]}" do
    before (:all) do
      @result = Sessions.new.login(e[:payload])
    end

    it "valida status code #{e[:code]}" do
      expect(@result.code).to eql e[:code]
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["error"]).to eql e[:error]
    end
  end
end
