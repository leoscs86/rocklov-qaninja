describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "kelmont@gmail.com", password: "scsleo86" }
    result = Sessions.new.login(payload)
    @dustin_id = result.parsed_response["_id"]
  end

  context "solicitar locacao" do
    before(:all) do
      result = Sessions.new.login({ email: "dustin@gmail.com", password: "scsleo86" })
      leo_id = result.parsed_response["_id"]

      fender = {
        thumbnail: Helpers::get_thumb("fender-sb.jpg"),
        name: "Fender Strato",
        category: "Cordas",
        price: "150",
      }

      MongoDB.new.remove_equipo(fender[:name], leo_id)

      result = Equipos.new.create(fender, leo_id)
      fender_id = result.parsed_response["_id"]

      @result = Equipos.new.booking(fender_id, @dustin_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
