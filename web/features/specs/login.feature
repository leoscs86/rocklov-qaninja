#language: pt

Funcionalidade: Login

    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "teste@teste.com" e "scsleo86"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:

            | email_input               | senha_input | mensagem_output                  |
            | leoscs86@gmail.com        | aaa111      | Usuário e/ou senha inválidos.    |
            | leonardo.1986@hotmail.com | scsleo86    | Usuário e/ou senha inválidos.    |
            | leonardo&gmail.com        | scsleo86    | Oops. Informe um email válido!   |
            |                           | scsleo86    | Oops. Informe um email válido!   |
            | leoscs86@gmail.com        |             | Oops. Informe sua senha secreta! |

