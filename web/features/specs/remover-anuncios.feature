#language: pt

Funcionalidade: Remover Anúncios
    Sendo um anúnciante que possui um equipamento indesejado
    Quero poder remover esse anúncio
    Para que eu possa manter o meu Dashboard atualizado

Contexto: Login
    * Login com "helmut@gmail.com" e "scsleo86"

      
    Cenário: Remover um anúncio

    Dado que eu tenho um anúncio indesejado:
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preço     | 50             |
            
    Quando eu solicito a exclusão deste item
    E confirmo a exclusão
    Então não devo ver este item no meu Dashboard

    @temp  
    Cenário: Desistir da exclusão

      Dado que eu tenho um anúncio indesejado:
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros    |
            | preço     | 100       |
    
    Quando eu solicito a exclusão deste item
    Mas não confirmo a solicitação
    Então esse item deve permanecer no meu Dashboard