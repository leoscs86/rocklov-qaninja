#language: pt

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenario: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome | email                    | senha    |
            | José | josesilva@ext.worten.net | scsleo86 |
        Então sou redirecionado para o Dashboard


    Esquema do Cenario: Tentativa de Cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:

            | nome_input | email_input             | senha_input | mensagem_output                  |
            |            | josesilva@ext.worten.pt | scsleo86    | Oops. Informe seu nome completo! |
            | José       |                         | scsleo86    | Oops. Informe um email válido!   |
            | José       | josesilva£ext.worten.pt | scsleo86    | Oops. Informe um email válido!   |
            | José       | josesilva%ext.worten.pt | scsleo86    | Oops. Informe um email válido!   |
            | José       | josesilva@ext.worten.pt |             | Oops. Informe sua senha secreta! |

